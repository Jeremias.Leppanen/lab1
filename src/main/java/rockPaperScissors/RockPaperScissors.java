package rockPaperScissors;

import java.util.Arrays;
import java.util.Random;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    	
    	boolean assertion = true; 
    	while(assertion) { //Initializing while-loop, which runs until user inputs "n".
    		System.out.printf("Let's play round %d%n", roundCounter);
    		int randomChoiceIndex = generateRandomChoice();
    		String randomChoice = rpsChoices.get(randomChoiceIndex);
    		
    		String inputChoice;
    		int inputChoiceIndex;
    		
    		while(true) {
    			inputChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
	    		inputChoiceIndex = rpsChoices.indexOf(inputChoice);
	    		
	    		if(inputChoiceIndex == -1) {
	    			System.out.printf("I do not understand %s. Could you try again?%n", inputChoice);
	    		} else {
	    			break;
	    		}
    		}
			
			System.out.printf("Human chose %s, computer chose %s. ", inputChoice, randomChoice);
			if(randomChoiceIndex == inputChoiceIndex) { //Case 1: Tie. Comparing indexes is easiest.
				System.out.println("It's a tie!");
			} else if((randomChoiceIndex + 1) % rpsChoices.size() == inputChoiceIndex) { //Case 2: Human wins.
				System.out.println("Human wins!");
				humanScore += 1;
			} else { //Case 3: Computer wins.
				System.out.println("Computer wins!");
				computerScore += 1;
    		} 
			
			/* The above "else if"-statement works because the elements in rpsChoices are ordered such that any
			 * choice loses to the choice to the right (with wrap-around support courtesy of modulo operator). 
			 * Use of ".size()"-method allows for expansion of rule-set of rock/paper/scissors to accommodate 
			 * more items.
			 */
			
			System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);
			
			while(true) {
				String criterion = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
				if(criterion.equals("n")) {
					System.out.println("Bye bye :)");
					assertion = false;
					break;
				} else if(criterion.equals("y")) {
					roundCounter += 1;
					break;
				} else {
					System.out.println("Please only input (y/n)! Do you wish to continue playing?");
				}		
			}
    	}	
    }
    
    Random rand = new Random(); //Supplementary method for randomizing.
    
    public int generateRandomChoice() {
    	
    	int ceiling = rpsChoices.size();
    	int randomInt = rand.nextInt(ceiling);
    	return randomInt;
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
